/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define Lcd_CS	GPIOB, GPIO_PIN_6
/* USER CODE END PD */

/*============================================================================*/
 /* USER CODE BEGIN 2 */
  St7920_SPI_Init(&hspi1);
  St7920_Write_Str("Test",4);
  /* USER CODE END 2 */
  
  
/* USER CODE BEGIN PFP */
void St7920_SPI_Init(SPI_HandleTypeDef *hspi);
void spi_lcd_send(SPI_HandleTypeDef *hspi, uint8_t Data, uint8_t RS);
ST7920_Write_Str (uint8_t *pData, uint16_t Size);
/* USER CODE END PFP */

/*=========================SPI_Seting=========================================*/
//static void MX_SPI1_Init(void)
//{
//
//  /* USER CODE BEGIN SPI1_Init 0 */
//
//  /* USER CODE END SPI1_Init 0 */
//
//  /* USER CODE BEGIN SPI1_Init 1 */
//
//  /* USER CODE END SPI1_Init 1 */
//  /* SPI1 parameter configuration*/
//  hspi1.Instance = SPI1;
//  hspi1.Init.Mode = SPI_MODE_MASTER;
//  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
//  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
//  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
//  hspi1.Init.CLKPhase = SPI_PHASE_2EDGE;
//  hspi1.Init.NSS = SPI_NSS_SOFT;
//  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
//  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
//  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
//  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
//  hspi1.Init.CRCPolynomial = 7;
//  hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
//  hspi1.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;
//  if (HAL_SPI_Init(&hspi1) != HAL_OK)
//  {
//    Error_Handler();
//  }
//  /* USER CODE BEGIN SPI1_Init 2 */
//
//  /* USER CODE END SPI1_Init 2 */
//
//}

/*============================================================================*/
/* USER CODE BEGIN 4 */
void St7920_SPI_Init(SPI_HandleTypeDef *hspi){
	HAL_Delay(100);
	spi_lcd_send(hspi, 0x20, 0);	 //b0010_0000
	spi_lcd_send(hspi, 0x20, 0);	 //b0010_0000
	spi_lcd_send(hspi, 0x0F, 0);     //b0000_1111
	spi_lcd_send(hspi, 0x01, 0);
	spi_lcd_send(hspi, 0x06, 0); 



}

void spi_lcd_send(SPI_HandleTypeDef *hspi, uint8_t Data, uint8_t RS){

	uint8_t buf[3];
    
	buf[0]= RS<<1 | 0xF8 ;//11111000
	buf[1]= Data & 0xF0;
	buf[2]= (uint8_t)(Data<<4  & 0xF0);
    
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_SET);
	HAL_SPI_Transmit(hspi, buf, 3, 10);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_RESET);
}
void ST7920_Write_Str (uint8_t *pData, uint16_t Size)
{
	while (Size > 0U){
		spi_lcd_send(*pData++, DATA);
		Size--;
	}
}
/* USER CODE END 4 */