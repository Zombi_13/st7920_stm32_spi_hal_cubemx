#ifndef ST7920_H_
#define ST7920_H_

#include "stm32f0xx_hal.h"
  
  /* -------------------------------------------------------------------------*/
typedef enum
{
  CMD = 0U,
  DATA
}St7920_DataType;

  /* -------------------------------------------------------------------------*/
void spi_lcd_send(uint8_t Data, uint8_t RS);
void St7920_SPI_Init(SPI_HandleTypeDef *hspi, GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
void ST7920_Write_Str (uint8_t *pData, uint16_t Size);

#endif /* ST7920_H_ */