#include <ST7920.h>

SPI_HandleTypeDef* 	hspi_St7920;
GPIO_TypeDef* 		CS_St7920_GPIOx;
uint16_t 			CS_St7920_GPIO_Pin;

void St7920_SPI_Init(SPI_HandleTypeDef *hspi, GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
	hspi_St7920			= hspi;
	CS_St7920_GPIOx 	= GPIOx;
	CS_St7920_GPIO_Pin 	= GPIO_Pin;

	HAL_Delay(100);
	spi_lcd_send(0x03, 0);	//b0010_0000
	spi_lcd_send(0x03, 0);	//b0010_0000
	spi_lcd_send(0x0E, 0); //b0000_1111
	spi_lcd_send(0x01, 0);
	spi_lcd_send(0x06, 0); //

}


void spi_lcd_send(uint8_t Data, St7920_DataType RS)
{
	uint8_t buf[3];

	buf[0]= RS<<1 | 0xF8 ;//11111000
	buf[1]= Data & 0xF0;
	buf[2]= (uint8_t)(Data<<4  & 0xF0);

	HAL_GPIO_WritePin(CS_St7920_GPIOx, CS_St7920_GPIO_Pin, GPIO_PIN_SET);
	HAL_SPI_Transmit(hspi_St7920, buf, 3, 10);
	HAL_Delay(10);
	HAL_GPIO_WritePin(CS_St7920_GPIOx, CS_St7920_GPIO_Pin, GPIO_PIN_RESET);
}

void ST7920_Write_Str (uint8_t *pData, uint16_t Size)
{
	while (Size > 0U){
		spi_lcd_send(*pData++, DATA);
		Size--;
	}
}